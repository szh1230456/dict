package com.example.demo;

import com.example.demo.controller.DictController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PutMapping;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDictApi {

    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(new DictController()).build();
    }

    @Test
    public void testGetPageDict() throws Exception {
        RequestBuilder request = null;

        request = get("/dict/page?page=0");
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));

        request = post("/dict/word")
                .param("word", "头")
                .param("classify", "BodyPart");
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        request = delete("/dict/word")
                .param("word","三叉神经")
                .param("classify","BodyPart");
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }
}
