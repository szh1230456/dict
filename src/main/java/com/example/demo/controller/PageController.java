package com.example.demo.controller;

import com.example.demo.entity.Dict;
import com.example.demo.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/dict")
public class PageController {

    @Autowired
    private DictService dictService;

    //列出所有的字典，返回到主页
    @RequestMapping(value="")
    public String dictList(@PageableDefault(value = 16, sort="id", direction = Sort.Direction.ASC) Pageable pageable,
                           Model model) {
        Page<Dict> dicts = dictService.findAll(pageable);
        model.addAttribute("dicts", dicts);
        //System.out.println(dicts.getTotalElements());
       // System.out.println(dicts.getTotalPages());
        model.addAttribute(dicts.getTotalPages());
        model.addAttribute(dicts.getTotalElements());
        return "index";
    }

    //添加一个词
    @PostMapping(value = "/word")
    public String addWord(String word, String category, String preferredName, int page) {
        dictService.addWordAndCategory(word, category, preferredName);
        return "redirect:/dict?page=" + page;

    }
    //修改原有的词
    @PostMapping(value = "/updateWord")
    public String updateWord(int id, String category, String preferredName, Integer page) {
        dictService.updateWordCategory(id, category, preferredName);
        if (page == null) {
            page = 0;
        }
        return "redirect:/dict?page=" + page;
    }

    //删除原有的词
    @GetMapping (value = "/delete")
    public String deleteWord(@RequestParam("word") String word){
        dictService.deleteByWord(word);
        return "redirect:/dict";
    }
    //模糊查询包含word的词
    @GetMapping(value = "/search")
    public String searchWordAndCategory(String word,
                             Model model,
                             @RequestParam(required = false) String category,
                             @PageableDefault(value = 16, sort="id", direction = Sort.Direction.ASC)
                                         Pageable pageable){
        Page<Dict> dicts;
        if("".equals(category)){
             dicts = dictService.findByWordLike(word,pageable);
        }else {
         dicts = dictService.findByWordLikeAndCategory(word, category, pageable);
        }
        model.addAttribute("dicts",dicts);
        model.addAttribute("searchWord", word);
        model.addAttribute("searchClassify", category);
        return "index";
    }

}
