package com.example.demo.controller;

import com.example.demo.entity.Dict;
import com.example.demo.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dict/rest")
public class DictController {
    @Autowired
    private DictService dictService;
    //获取所有列表
    @GetMapping("/all")
    public List<Dict> getAllDict() {
        return dictService.findAll();
    }

    //分页
    @GetMapping("/page")
    public Page<Dict> getPageDict(@PageableDefault(value=20, sort="id", direction = Sort.Direction.ASC) Pageable pageable) {
        return dictService.findAll(pageable);
    }

  //查询含有WORD的所有词
    @GetMapping("/search/{word}")
    public List<Dict> searchWord(@PathVariable String word) {
        return dictService.findByWordLike(word);
    }

    //查询word是否包含在字典中
    @GetMapping("/isin/{word}")
    public boolean isInWord(@PathVariable String word){
        Dict dict = dictService.findByWord(word);
        return dict != null;
    }

    //修改word
    @PutMapping(value = "/update")
    public Dict updateWordClassify(@RequestBody Map<String, String> body,
                                   @RequestParam("id") Integer id) {
        String preferredName = body.get("preferredName");
        String category = body.get("category");
        return dictService.updateWordCategory(id,category,preferredName);
    }
    //添加
    @PostMapping(value = "/word")
    public Dict addWordCategory(@RequestParam("word") String word,
                                @RequestParam("category") String category,
                                @RequestParam("id") Integer id,
                                @RequestParam("preferredName") String preferredName){
        boolean flag=isInWord(word);
        if (!flag) {
          return  dictService.updateWordCategory(id,category,preferredName);
        }else {
              Dict dict=new Dict();
              dict.setCategory(category);
              dict.setWord(word);
              return dictService.addWordAndCategory(word, category, preferredName);
        }
    }
    //删除
    @DeleteMapping (value = "/delete")
    public void deleteByWord(@RequestBody Map<String, String> body){
        String word = body.get("word");
        dictService.deleteByWord(word);
    }

}
