package com.example.demo.repository;

import com.example.demo.entity.Dict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

public interface DictRepository extends JpaRepository<Dict, Integer> {

    Dict findByWord(@Param("word") String word);

    @Query("SELECT d FROM Dict d WHERE d.word like CONCAT('%', ?1, '%')")
    List<Dict> findByWordLike(String word);

    @Transactional()
    void deleteByWord(@Param("word") String word);

    @Query("SELECT d FROM Dict d WHERE d.word like CONCAT('%', ?1, '%')")
    Page<Dict> findByWordLike(String word, Pageable pageable);

    @Query("SELECT d FROM Dict d WHERE d.word like CONCAT('%', ?1, '%') and d.category like ?2")
    Page<Dict> findByWordLikeAndCategory(String word, String category, Pageable pageable);


}
