package com.example.demo.service;

import com.example.demo.entity.Dict;
import com.example.demo.repository.DictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DictService {
    @Autowired
    private DictRepository dictRepository;

    public List<Dict> findAll() {
        return dictRepository.findAll();
    }

    public Dict findByWord(String word) { return dictRepository.findByWord(word); }

    public List<Dict> findByWordLike(String word) {
        return dictRepository.findByWordLike(word);
    }

    public Page<Dict> findByWordLike(String word, Pageable pageable) {
        return dictRepository.findByWordLike(word, pageable);
    }

    public Page<Dict> findByWordLikeAndCategory(String word, String category, Pageable pageable) {
        return dictRepository.findByWordLikeAndCategory(word, category, pageable);
    }


    public void deleteByWord(String word) {
        dictRepository.deleteByWord(word);
    }

    public Dict updateWordCategory(Integer id, String category ,String preferredName) {
        Optional<Dict> dict_o = dictRepository.findById(id);
        if (dict_o.isPresent()) {
            Dict dict = dict_o.get();
            dict.setCategory(category);
            dict.setPreferredName(preferredName);
            return dictRepository.save(dict);
        } else {
            return null;
        }
    }

    public Dict addWordAndCategory(String word,String category, String preferredName){
        Dict dict = findByWord(word);
        if (dict != null) {
            dict.setCategory(category);
            dict.setPreferredName(preferredName);
        }else {
            dict = new Dict();
            dict.setWord(word);
            dict.setCategory(category);
            dict.setPreferredName(preferredName);
        }
        return dictRepository.save(dict);
    }

    public Page<Dict> findAll(Pageable pageable) {
        return dictRepository.findAll(pageable);
    }
}
