$(function() {
    $('#example').DataTable(
        {
            "searching": false
       // "bPaginate": false,
       // "language": {
       //     "info": "",
        // }
    });

    $('.table-hover>td:nth-child(5)>a:first-child')
        .click(function(e) {
            // e.preventDefault();
            const that = $(this);

            let category = that.parent().parent().children('td:nth-child(3)');
            let preferredName = that.parent().parent().children('td:nth-child(4)');

            let category_input = category.text();
            category.text("");
            let category_input_dom = $('<input/>')
                .addClass('.input-class')
                .val(category_input);
            // classify.append('<input value="'+ classify_input +'" />');
            category.append(category_input_dom);

            let preferred_input = preferredName.text();
            preferredName.text("");
            let preferred_input_dom = $('<input/>')
                .addClass('.input-class')
                .val(preferred_input);
            // preferredName.append('<input value="'+ preferred_input +'" />');
            preferredName.append(preferred_input_dom);


            let confirm_button = $('<button/>').addClass('btn btn-success btn-sm').css({ marginRight: "2px" })
                .text('确认');
            let cancel_button = $('<button/>').addClass('btn btn-info btn-sm')
                .text('取消');
            let parent = that.parent();
            parent.children('a').hide();
            parent.append(confirm_button, cancel_button);

            confirm_button.click(function(e){
                let id = that.parent().parent().children('td:nth-child(1)').text();
                let new_category = category_input_dom.val();
                let new_preferred_name = preferred_input_dom.val();

                $.ajax({
                    url: '/dict/updateWord',
                    method: 'post',
                    data: {
                        id: id,
                        category: new_category,
                        preferredName: new_preferred_name
                    },
                    success: function() {
                        location.reload(true);
                        alert("修改成功！")
                    }
                })
            });

            cancel_button.click(function(e) {
                category.empty();
                preferredName.empty();
                parent.children('.btn').remove();
                category.text(category_input);
                preferredName.text(preferred_input);
                parent.children('a').show();
            });
            console.log('...');
        });

    // 删除事件
    $('.table-hover>td:nth-child(5)>a:last-child')
        .click(function() {
            let that = $(this);
            let word = that.attr('id');
            // noinspection JSAnnotator
            let deleteUrl = `/dict/delete?word=${word}`;

            $.ajax({
                url: deleteUrl,
                method: 'GET',
                success: function () {
                    location.reload(true);
                    alert("删除成功")
                },
                error: function () {
                    alert("删除失败")
                }
            })
        });

    $('#find').submit(function(e) {
        e.preventDefault();
        const that = $(this);
        let data = that.serialize();
        let w = $('#searchWord').val();
        let c = $('#searchCategory').val();
        $.ajax({
            url: that.attr('action'),
            data: data,
            method: 'get',
            success: function() {
                // noinspection JSAnnotator
                window.location.href = `/dict/search/?word=${w}&category=${c}`
            }
        })
    })

});